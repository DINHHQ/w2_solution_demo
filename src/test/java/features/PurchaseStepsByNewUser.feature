@PurchaseByNewUser
Feature: Purchase Product at Front by New User

  Scenario Outline: Create New User
    #---- Login
    Given Click Button Resgiter User
    #----  UserRegistRegulation
    When Click Accept Policy
    #----   UserRegistInput
    When I input to "ctl00_ContentPlaceHolder1_tbUserName1" textbox with data "<Surname>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserName2" textbox with data "<Name>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserNameKana1" textbox with data "<Surname-Kana>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserNameKana2" textbox with data "<Name-Kana>"
    When I choose to "ctl00_ContentPlaceHolder1_ddlUserBirthYear" drowdownlist with data "1992"
    When I choose to "ctl00_ContentPlaceHolder1_ddlUserBirthMonth" drowdownlist with data "12"
    When I choose to "ctl00_ContentPlaceHolder1_ddlUserBirthDay" drowdownlist with data "12"
    When I input to "ctl00_ContentPlaceHolder1_tbUserMailAddr" textbox with random data "<MailAddress>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserMailAddrConf" textbox with random data "<MailAddressConfirm>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserZip1" textbox with data "<Zip1>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserZip2" textbox with data "<Zip2>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserAddr2" textbox with data "<City>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserAddr3" textbox with data "<Address>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserAddr4" textbox with data "<Apartment>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserTel1" textbox with data "<Tel1>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserTel2" textbox with data "<Tel2>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserTel3" textbox with data "<Tel3>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserPassword" textbox with data "<Password>"
    When I input to "ctl00_ContentPlaceHolder1_tbUserPasswordConf" textbox with data "<PasswordConfirm>"
    When  Click button Next User
    #------UserRegistConfirm
    When  Click button Next Confirm
    Examples: 
      | Surname | Name    | Surname-Kana | Name-Kana | MailAddress | MailAddressConfirm | Zip1 | Zip2 | City | Address | Apartment | Tel1 | Tel2 | Tel3 | MobTel1 | MobTel2 | MobTel3 | Quantily | Point | Coupon | Payment |Password|PasswordConfirm|
      | ｗ２      | vn-dinh | カナ           | カナ        | @w2s.xyz    | @w2s.xyz           |  105 | 0004 | 港区新橋 | １－７－６   | 美スズビル６Ｆ   |   03 | 5568 | 4368 |      03 |    5568 |    4368 |        2 |    50 | TT06   | 代金引換    |password123|password123|

  Scenario Outline: Purchase Product
    #---- Login
  #  Given Login with userID "qqqq@w2s.xyz" and password "password123"
 #   And Click Login Button
    #---- HomePage
    Given Click Search Icon
    #----ProductList
    Given Click product "ss02"
    #----ProductDetail
    Given Choose purchase product normal
    #----CartList
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_rCart_ctl00_tbProductCount" textbox with data "<Quantily>"
    And I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOrderPointUse" textbox with data "<Point>"
    And I check "ctl00_ContentPlaceHolder1_rCartList_ctl00_rblCouponInputMethod_1"at クーポンを使う
    And I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbCouponCode" textbox with data "<Coupon>"
    And Click button Next
    # Then Compare User move to Order Combine Select List Page
    #----OrderShipping
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerName1" textbox with data "<Surname>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerName2" textbox with data "<Name>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerNameKana1" textbox with data "<Surname-Kana>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerNameKana2" textbox with data "<Name-Kana>"
    When I choose to "ctl00_ContentPlaceHolder1_rCartList_ctl00_ddlOwnerBirthYear" drowdownlist with data "1992"
    When I choose to "ctl00_ContentPlaceHolder1_rCartList_ctl00_ddlOwnerBirthMonth" drowdownlist with data "12"
    When I choose to "ctl00_ContentPlaceHolder1_rCartList_ctl00_ddlOwnerBirthDay" drowdownlist with data "12"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerMailAddr" textbox with random data "<MailAddress>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerMailAddrConf" textbox with random data "<MailAddressConfirm>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerZip1" textbox with data "<Zip1>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerZip2" textbox with data "<Zip2>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerAddr2" textbox with data "<City>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerAddr3" textbox with data "<Address>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerAddr4" textbox with data "<Apartment>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerTel1_1" textbox with data "<Tel1>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerTel1_2" textbox with data "<Tel2>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerTel1_3" textbox with data "<Tel3>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerTel2_1" textbox with data "<MobTel1>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerTel2_2" textbox with data "<MobTel2>"
    When I input to "ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOwnerTel2_3" textbox with data "<MobTel3>"
    And Click button Next
    #----OrderPayment
    When I choose payment radio button with data "<Payment>"
    And Click button Next
    #----OrderConfirm
    And Click button Next

    Examples: 
      | Surname | Name    | Surname-Kana | Name-Kana | MailAddress | MailAddressConfirm | Zip1 | Zip2 | City | Address | Apartment | Tel1 | Tel2 | Tel3 | MobTel1 | MobTel2 | MobTel3 | Quantily | Point | Coupon | Payment |
      | ｗ２      | vn-dinh | カナ           | カナ        | @w2s.xyz    | @w2s.xyz           |  105 | 0004 | 港区新橋 | １－７－６   | 美スズビル６Ｆ   |   03 | 5568 | 4368 |      03 |    5568 |    4368 |        2 |    50 | TT06   | 代金引換    |

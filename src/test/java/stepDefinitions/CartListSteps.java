package stepDefinitions;

import interfaces.CartListPageUI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.AbstractPageObject;
import pageObjects.CartListPageObject;
import pageObjects.PageManagerDriver;

import common.AbstractPage;
import common.AbstractTest;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberConfig.Hooks;

public class CartListSteps extends AbstractTest {
	WebDriver driver;
	private CartListPageObject cartListPage;
	private AbstractPageObject abstractPage;


	public CartListSteps() {
		driver = Hooks.openBrowser();
		cartListPage = PageManagerDriver.getCartListPage(driver);
	}

	 @When("^I input to textbox with data point$")
	 public void iInputToTextboxWithDataPoint() throws Exception {
	WebElement element = driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_rCartList_ctl00_tbOrderPointUse']"));
	AbstractPage.setAttributeInDOM(driver, element, "value", "10");
	Thread.sleep(1000);
	 }

	@When("^I check \"(.*?)\"at クーポンを使う$")
	public void iCheck入力するAtクーポンを使う(String checkboxAddr) throws Exception {
		WebElement element = driver.findElement(By.xpath("//input[@id='ctl00_ContentPlaceHolder1_rCartList_ctl00_rblCouponInputMethod_1']"));
		AbstractPage.executeForWebElement(driver,element);
//		cartListPage.chooseRadioButton(checkboxAddr);
		Thread.sleep(1000);

	}

	@Then("^Compare User move to Order Combine Select List Page$")
	public void compareUserMoveToOrderCombineSelectListPage() {
//		if (verifyTrue(cartListPage.checkDisplay())) {
//			abstractPage.clickToElement(driver, CartListPageUI.BUTTON_COMBINE);
//
//		} else {
//			 try {
//			 if (verifyTrue(cartListPage.checkDisplay())) {
//			 abstractPage.clickToElement(driver, CartListPageUI.BUTTON_COMBINE);
//			
//			 }
//			
//			 } catch (Exception e) {
//			 // TODO: handle exception
//			 }
//			
//
//		}

		 try {
		 if (verifyTrue(cartListPage.checkDisplay())) {
		 abstractPage.clickToElement(driver, CartListPageUI.BUTTON_COMBINE);
		
		 }
		
		 } catch (Exception e) {
		 // TODO: handle exception
		 }
		
	}
	


}

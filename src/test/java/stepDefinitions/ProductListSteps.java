package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.PageManagerDriver;
import pageObjects.ProductListPageObject;
import cucumber.api.java.en.Given;
import cucumberConfig.Hooks;

public class ProductListSteps {
	
	WebDriver driver;
	WebDriverWait wait;
	private  ProductListPageObject productList;

	public ProductListSteps() {
		driver = Hooks.openBrowser();
		productList = PageManagerDriver.getProductListPage(driver);
	}
	
	
	@Given("^Click product \"(.*?)\"$")
	public void clickProduct(String product) {
		productList.clickProduct();

	}
	

}

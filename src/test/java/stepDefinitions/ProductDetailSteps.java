package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.PageManagerDriver;
import pageObjects.ProductDetailPageObject;
import cucumber.api.java.en.Given;
import cucumberConfig.Hooks;

public class ProductDetailSteps {
	
	WebDriver driver;
	WebDriverWait wait;
	private ProductDetailPageObject productDetail;

	public ProductDetailSteps() {
		driver = Hooks.openBrowser();
		productDetail = PageManagerDriver.getProductDetailPage(driver);
	}
	
	@Given("^Choose purchase product normal$")
	public void choosePurchaseProductNormal()   {
		
		productDetail.clickButtonNormal();

	}

}

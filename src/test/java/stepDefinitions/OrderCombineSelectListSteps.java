package stepDefinitions;

import org.openqa.selenium.WebDriver;

import pageObjects.OrderCombineSelectListPageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.When;
import cucumberConfig.Hooks;

public class OrderCombineSelectListSteps {
	
	WebDriver driver;
	private static OrderCombineSelectListPageObject orderCombineSelectListPage;

	
	
	
	public OrderCombineSelectListSteps() {
		driver = Hooks.openBrowser();
		orderCombineSelectListPage = PageManagerDriver.getOrderCombineSelectListPage(driver);
	}
	
	@When("^Check radioBtn\"(.*?)\" 同梱対象注文$")
	public void checkRadioBtn同梱対象注文(String RadioBtnAddr) {
		orderCombineSelectListPage.selectRadioBtn(RadioBtnAddr);
	}
	

	
	

	
	
	

	

}

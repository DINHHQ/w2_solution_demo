package stepDefinitions;

import org.openqa.selenium.WebDriver;

import pageObjects.OrderShippingPageObject;
import pageObjects.PageManagerDriver;
import cucumberConfig.Hooks;

public class ShippingPageSteps {
	
	WebDriver driver;
	private OrderShippingPageObject shippingPageObject;
	
	public ShippingPageSteps() {
		driver = Hooks.openBrowser();
		shippingPageObject = PageManagerDriver.getOrderShippingPage(driver);
	}

}

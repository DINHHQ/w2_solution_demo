package stepDefinitions;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.AbstractPageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.When;
import cucumberConfig.Hooks;

public class AbstractPageSteps {

	static WebDriver driver;
	private static AbstractPageObject abstractPage;
//	private AbstractTest  abstractTest ;
//	abstractTest = new AbstractTest();
	int addressMail = randomData();

	public AbstractPageSteps() {
		driver = Hooks.openBrowser();
		abstractPage = PageManagerDriver.getAbstractPage(driver);
//		abstractTest = new AbstractTest();


	}

	@When("^I input to \"(.*?)\" textbox with data \"(.*?)\"$")
	public void iInputToTextboxWithData(String textboxAddr, String dataTest) throws Exception {
		abstractPage.inputToDynamicTextBox(textboxAddr, dataTest);
		Thread.sleep(1000);

	}

	@When("^I input to \"(.*?)\" textbox with random data \"(.*?)\"$")
	public void iInputToTextboxWithRandomData(String textboxAddr, String dataTest) {
//		addressMail = abstractTest.randomData();
		dataTest = addressMail + dataTest;
		abstractPage.inputToDynamicTextBox(textboxAddr, dataTest);

	}
	
	@When("^Click button Next$")
	public void clickButtonNext() throws Exception   {
		WebElement element = driver.findElement(By.xpath("//div[@class='btmbtn above cartstep']//a[@class='btn btn-success']"));
		abstractPage.executeForWebElement(driver, element);
        Thread.sleep(1000);


	}
	

	
	@When("^I choose to \"(.*?)\" drowdownlist with data \"(.*?)\"$")
	public void iChooseToDrowdownlistWithData(String drowDownListAddr, String dataTest)  {
		abstractPage.selectDropDownListDate(drowDownListAddr, dataTest);

	}
	
	@When("^Click button Next User$")
	public void clickButtonNextUser() throws Exception   {
		WebElement element = driver.findElement(By.xpath("//a[@id='ctl00_ContentPlaceHolder1_lbConfirm']"));
		abstractPage.executeForWebElement(driver, element);
        Thread.sleep(1000);


	}
	
	@When("^Click button Next Confirm$")
	public void clickButtonNextConfirm() throws Exception   {
		WebElement element = driver.findElement(By.xpath("//a[@id='ctl00_ContentPlaceHolder1_lbSend']"));
		abstractPage.executeForWebElement(driver, element);
        Thread.sleep(1000);


	}
	
	

	
	public int randomData() {
		Random ran = new Random();
		int random = ran.nextInt(9999);
		return random;

	}


}

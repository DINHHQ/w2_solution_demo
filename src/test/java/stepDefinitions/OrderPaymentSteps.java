package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.OrderPaymentPageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.When;
import cucumberConfig.Hooks;

public class OrderPaymentSteps {
	
	WebDriver driver;
	private OrderPaymentPageObject orderPaymentPage;
	
	public OrderPaymentSteps() {
		driver = Hooks.openBrowser();
		orderPaymentPage = PageManagerDriver.getOrderPaymentPage(driver);
	}
	
	@When("^I choose payment radio button with data \"(.*?)\"$")
	public void iChoosePaymentToRadioButtonWithData(String radioAddr)  {

		orderPaymentPage.choosePayment(radioAddr);
//		WebElement element = driver.findElement(By.xpath("//label[text()='代金引換']"));
//		element.click();
		

	}
}

package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.HomePageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.Given;
import cucumberConfig.Hooks;

public class HomePageSteps {
	
	WebDriver driver;
	WebDriverWait wait;
	private HomePageObject homePage;

	public HomePageSteps() {
		driver = Hooks.openBrowser();
		homePage = PageManagerDriver.getHomePage(driver);
	}
	
	@Given("^Click Search Icon$")
	public void clickSearchIcon()  {
		homePage.clickSearchIcon();

	}
	



}

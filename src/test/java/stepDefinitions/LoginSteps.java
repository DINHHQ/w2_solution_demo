package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.LoginPageObject;
import pageObjects.PageManagerDriver;
import cucumber.api.java.en.Given;
import cucumberConfig.Hooks;

public class LoginSteps {

	WebDriver driver;
	WebDriverWait wait;
	private LoginPageObject loginPage;

	public LoginSteps() {
		driver = Hooks.openBrowser();
		loginPage = PageManagerDriver.getLoginPage(driver);
	}

	@Given("^Login with userID \"(.*?)\" and password \"(.*?)\"$")
	public void loginWithUserIDAndPassword(String userID, String password) {

		loginPage.sendKeyDynamic(userID, "ctl00_ContentPlaceHolder1_tbLoginIdInMailAddr");
		loginPage.sendKeyDynamic(password, "ctl00_ContentPlaceHolder1_tbPassword");

	}

	@Given("^Click Login Button$")
	public void clickLoginButton() throws Exception {
		loginPage.clickLoginButton();
	}
	
	@Given("^Click Button Resgiter User$")
	public void clickButtonRegUser() throws Exception {
		loginPage.clickCreateUserBtn();
	}

}

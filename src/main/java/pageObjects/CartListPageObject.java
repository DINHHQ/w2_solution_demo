package pageObjects;

import interfaces.AbstractPageIU;
import interfaces.CartListPageUI;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class CartListPageObject extends AbstractPage {

	WebDriver driver;

	public CartListPageObject(WebDriver mapdriver) {
		driver = mapdriver;

	}

	// public void sendKeyDynamic(String textboxAddr, String locator) {
	// waitForControlVisible(driver, AbstractPageIU.DYNAMIC_LINKS, locator);
	// sendkeyToElement(driver, AbstractPageIU.DYNAMIC_LINKS, textboxAddr, locator);
	// }
	//

	public void chooseRadioButton(String checkBoxAddr) {
		waitForControlVisible(driver, AbstractPageIU.DYNAMIC_LINKS, checkBoxAddr);
		clickToElement(driver, AbstractPageIU.DYNAMIC_LINKS, checkBoxAddr);
	}

	public boolean checkDisplay(){
		waitForControlVisible(driver, CartListPageUI.BUTTON_COMBINE);
		return isControlDisplayed(driver, CartListPageUI.BUTTON_COMBINE);

	}
}

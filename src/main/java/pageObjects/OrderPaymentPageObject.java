package pageObjects;

import interfaces.OrderPaymentPageUI;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class OrderPaymentPageObject extends AbstractPage {
	
	WebDriver driver;
	
	public OrderPaymentPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public void choosePayment(String radioAddr) {
		waitForControlVisible(driver, OrderPaymentPageUI.DYNAMIC_RADIO_BUTTON_PAYMENT,radioAddr);
		clickToElement(driver, OrderPaymentPageUI.DYNAMIC_RADIO_BUTTON_PAYMENT,radioAddr);

		
	}

}

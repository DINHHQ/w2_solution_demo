package pageObjects;

import interfaces.ProductDetailPageUI;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class ProductDetailPageObject extends AbstractPage {
	
	WebDriver driver;
	

	
	public ProductDetailPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public CartListPageObject clickButtonNormal(){
		waitForControlVisible(driver, ProductDetailPageUI.BUTTON_NORMAL);
		clickToElement(driver, ProductDetailPageUI.BUTTON_NORMAL);
		return PageManagerDriver.getCartListPage(driver);
		

	}
	
	
	
	

}

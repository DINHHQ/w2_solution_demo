package pageObjects;

import org.openqa.selenium.WebDriver;

public class PageManagerDriver {

	private static LoginPageObject loginPage;
	private static ProductListPageObject productListPage;
	private static HomePageObject homePage;
	private static ProductDetailPageObject productDetailPage;
	private static CartListPageObject cartListPage;
	private static OrderShippingPageObject orderShippingPage;
	private static AbstractPageObject abstractPgae;
	private static OrderPaymentPageObject orderPaymentPage;
	private static OrderCompletePageObject orderCompletePage;
	private static UserRegistInputPageObject userRegistInputPage;
	private static UserRegistRegulationPageObject userRegistRegulationPage;
	private static OrderCombineSelectListPageObject orderCombineSelectListPage;

	public static LoginPageObject getLoginPage(WebDriver driver) {
		if (loginPage == null) {
			return new LoginPageObject(driver);
		}
		return loginPage;
	}

	public static ProductListPageObject getProductListPage(WebDriver driver) {
		if (productListPage == null) {
			return new ProductListPageObject(driver);
		}
		return productListPage;
	}

	public static HomePageObject getHomePage(WebDriver driver) {
		if (homePage == null) {
			return new HomePageObject(driver);
		}
		return homePage;
	}

	public static ProductDetailPageObject getProductDetailPage(WebDriver driver) {
		if (productDetailPage == null) {
			return new ProductDetailPageObject(driver);
		}
		return productDetailPage;
	}

	public static CartListPageObject getCartListPage(WebDriver driver) {
		if (cartListPage == null) {
			return new CartListPageObject(driver);
		}
		return cartListPage;
	}

	public static OrderShippingPageObject getOrderShippingPage(WebDriver driver) {
		if (orderShippingPage == null) {
			return new OrderShippingPageObject(driver);
		}
		return orderShippingPage;
	}

	public static AbstractPageObject getAbstractPage(WebDriver driver) {
		if (abstractPgae == null) {
			return new AbstractPageObject(driver);
		}
		return abstractPgae;
	}

	public static OrderPaymentPageObject getOrderPaymentPage(WebDriver driver) {
		if (orderPaymentPage == null) {
			return new OrderPaymentPageObject(driver);
		}
		return orderPaymentPage;
	}
	
	public static UserRegistRegulationPageObject getUserRegistRegulationPage(WebDriver driver) {
		if (userRegistRegulationPage == null) {
			return new UserRegistRegulationPageObject(driver);
		}
		return userRegistRegulationPage;
	}
	
	public static UserRegistInputPageObject getuserRegistInputPage(WebDriver driver) {
		if (userRegistInputPage == null) {
			return new UserRegistInputPageObject(driver);
		}
		return userRegistInputPage;
	}
	
	public static OrderCombineSelectListPageObject getOrderCombineSelectListPage(WebDriver driver) {
		if (orderCombineSelectListPage == null) {
			return new OrderCombineSelectListPageObject(driver);
		}
		return orderCombineSelectListPage;
	}
	
	


}

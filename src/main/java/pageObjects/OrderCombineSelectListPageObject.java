package pageObjects;


import interfaces.OrderCombineSelectListUI;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class OrderCombineSelectListPageObject extends AbstractPage {

	WebDriver driver;

	public OrderCombineSelectListPageObject(WebDriver mapdriver) {
		driver = mapdriver;

	}
	
	public void selectRadioBtn( String RadioBtnAddr ) {
		waitForControlVisible(driver, OrderCombineSelectListUI.RADIO_BTN, RadioBtnAddr);
		clickToElement(driver, OrderCombineSelectListUI.RADIO_BTN, RadioBtnAddr);
		
	}



}

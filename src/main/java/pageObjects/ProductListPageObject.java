package pageObjects;

import interfaces.ProductListPageUI;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class ProductListPageObject extends AbstractPage {
	
	WebDriver driver;
	
	public ProductListPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public ProductDetailPageObject clickProduct(){
		waitForControlVisible(driver, ProductListPageUI.PRODUCT);
		clickToElement(driver,  ProductListPageUI.PRODUCT);
		return PageManagerDriver.getProductDetailPage(driver);
	}
	


}

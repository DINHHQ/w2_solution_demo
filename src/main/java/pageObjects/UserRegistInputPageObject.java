package pageObjects;

import interfaces.AbstractPageIU;
import interfaces.LoginPageUI;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.AbstractPage;
import cucumber.api.java.en.When;

public class UserRegistInputPageObject extends AbstractPage {
	
	WebDriver driver;

	
	public UserRegistInputPageObject(WebDriver mapdriver) {
		driver = mapdriver;
	}
	
	public void sendKeyDynamic(String customerName, String locator) {
		waitForControlVisible(driver, AbstractPageIU.DYNAMIC_LINKS, locator);
		sendkeyToElement(driver, AbstractPageIU.DYNAMIC_LINKS, customerName, locator);
	}
	

	
	public HomePageObject clickLoginButton() {
		waitForControlVisible(driver, LoginPageUI.LOGIN_BTN);
		clickToElement(driver, LoginPageUI.LOGIN_BTN);
		return PageManagerDriver.getHomePage(driver);
		
	}
	
	
	
}

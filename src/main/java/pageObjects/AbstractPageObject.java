package pageObjects;

import interfaces.AbstractPageIU;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class AbstractPageObject extends AbstractPage {
	
	WebDriver driver;
	
	public AbstractPageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public void inputToDynamicTextBox( String textboxAddr, String dataTest ) {
		waitForControlVisible(driver, AbstractPageIU.DYNAMIC_LINKS, textboxAddr);
		sendkeyToElement(driver, AbstractPageIU.DYNAMIC_LINKS, dataTest, textboxAddr);
		
	}
	
	public void selectDropDownListDate( String DropDownListAddr, String dataTest ) {
		waitForControlVisible(driver, AbstractPageIU.DYNAMIC_DROPDOWNLIST, DropDownListAddr);
		selectItemInDropdown(driver, AbstractPageIU.DYNAMIC_DROPDOWNLIST,dataTest, DropDownListAddr);
		
	}
	
	
	
	
	

	

}

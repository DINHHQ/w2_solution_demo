package pageObjects;

import interfaces.AbstractPageIU;
import interfaces.LoginPageUI;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class LoginPageObject extends AbstractPage {
	
	WebDriver driver;
	
	public LoginPageObject(WebDriver mapdriver) {
		driver = mapdriver;
	}
	
	public void sendKeyDynamic(String customerName, String locator) {
		waitForControlVisible(driver, AbstractPageIU.DYNAMIC_LINKS, locator);
		sendkeyToElement(driver, AbstractPageIU.DYNAMIC_LINKS, customerName, locator);
	}
	
	public HomePageObject clickLoginButton() {
		waitForControlVisible(driver, LoginPageUI.LOGIN_BTN);
		clickToElement(driver, LoginPageUI.LOGIN_BTN);
		return PageManagerDriver.getHomePage(driver);
		
	}
	
	public UserRegistRegulationPageObject clickButtonRegUser() {
		waitForControlVisible(driver, LoginPageUI.CREATE_USER_BTN);
		clickToElement(driver, LoginPageUI.CREATE_USER_BTN);
		return PageManagerDriver.getUserRegistRegulationPage(driver);
		
	}
	
	public UserRegistRegulationPageObject clickCreateUserBtn() {
		waitForControlVisible(driver, LoginPageUI.CREATE_USER_BTN);
		clickToElement(driver, LoginPageUI.CREATE_USER_BTN);
		return PageManagerDriver.getUserRegistRegulationPage(driver);
		
	}
	
	
}

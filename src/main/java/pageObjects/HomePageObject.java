package pageObjects;

import interfaces.HomePageUI;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class HomePageObject extends AbstractPage {
	
	WebDriver driver;
	
	public HomePageObject(WebDriver mapdriver) {
		driver = mapdriver;
		
	}
	
	public ProductListPageObject clickSearchIcon(){
		waitForControlVisible(driver, HomePageUI.SEARCH_ICON);
		clickToElement(driver, HomePageUI.SEARCH_ICON);
		return PageManagerDriver.getProductListPage(driver);
	}
	
	


}

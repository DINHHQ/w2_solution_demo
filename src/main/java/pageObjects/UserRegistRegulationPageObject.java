package pageObjects;

import interfaces.UserRegistRegulationUI;

import org.openqa.selenium.WebDriver;

import common.AbstractPage;

public class UserRegistRegulationPageObject extends AbstractPage {
	
	WebDriver driver;
	
	public UserRegistRegulationPageObject(WebDriver mapdriver) {
		driver = mapdriver;
	}
	

	
	public UserRegistInputPageObject clickAcceptPolity() {
		waitForControlVisible(driver, UserRegistRegulationUI.AGREE_BTN_TERM);
		clickToElement(driver, UserRegistRegulationUI.AGREE_BTN_TERM);
		return PageManagerDriver.getuserRegistInputPage(driver);
		
	}
	
	
	
}

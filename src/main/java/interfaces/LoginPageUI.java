package interfaces;

public class LoginPageUI {
	
	public static final String USERID_TEXTBOX="//input[@id='ctl00_ContentPlaceHolder1_tbLoginIdInMailAddr']";
	public static final String PASSWORD_TEXTBOX="//input[@id='ctl00_ContentPlaceHolder1_tbPassword']";
	public static final String LOGIN_BTN="//a[@id='ctl00_ContentPlaceHolder1_lbLogin']";
	public static final String CREATE_USER_BTN="//a[text()='会員登録をする']";


}
